package com.example.jagios.constraintanim

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.transition.TransitionManager
import kotlinx.android.synthetic.main.activity_main_expanded.*

class MainActivity : AppCompatActivity() {

    val setExpanded:ConstraintSet = ConstraintSet()
    val setColapsed:ConstraintSet = ConstraintSet()

    var expanded:Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_expanded)

        setExpanded.clone(root)
        setColapsed.clone(this, R.layout.activity_main)

        time.text = "2 horas"
        today.text = "2 de Enero"

        des.text = "La mejor pelicula"
        name.text = "Acuaman"
        img.setImageURI("https://cdn.movieweb.com/img.teasers.posters/FIDKoDGFk1FqHD_372_a/Aquaman.jpg")

        btnInfo.setOnClickListener{
            TransitionManager.beginDelayedTransition(root)
            if(expanded) setColapsed.applyTo(root)
            else setExpanded.applyTo(root)

            expanded = !expanded
        }

    }
}
